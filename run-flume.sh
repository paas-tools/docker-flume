#!/bin/bash

# from https://gitlab.cern.ch/ai/it-puppet-module-flume/blob/qa/code/templates/etc/flume-ng/conf/flume-env.sh.erb
JAVA_DNS_SETTINGS="-Dnetworkaddress.cache.ttl=1 -Dnetworkaddress.cache.negative.ttl=0 -Dsun.net.inetaddr.ttl=1 -Dsun.net.inetaddr.negative.ttl=0"

# Set Java memory setting from container memory limits. From https://github.com/openshift/jenkins/blob/master/2/contrib/s2i/run
CONTAINER_MEMORY_IN_BYTES=`cat /sys/fs/cgroup/memory/memory.limit_in_bytes`
DEFAULT_MEMORY_CEILING=$((2**40-1))
if [ "${CONTAINER_MEMORY_IN_BYTES}" -lt "${DEFAULT_MEMORY_CEILING}" ]; then

    if [ -z $CONTAINER_HEAP_PERCENT ]; then
        LOW_MEMORY_THRESHOLD=$((100*2**20)) # 100M
        if [ "${CONTAINER_MEMORY_IN_BYTES}" -lt "${LOW_MEMORY_THRESHOLD}" ]; then
            # If the user is not actively tailoring environment variables to adjust memory use,
            # then tailor the runtime for limited memory automatically.
            CONTAINER_HEAP_PERCENT=0.40
            LOW_MEMORY_PARAMS="-Xss500K"
        else
            CONTAINER_HEAP_PERCENT=0.50
        fi
    fi
    
    CONTAINER_MEMORY_IN_MB=$((${CONTAINER_MEMORY_IN_BYTES}/1024**2))
    CONTAINER_HEAP_MAX=$(echo "${CONTAINER_MEMORY_IN_MB} ${CONTAINER_HEAP_PERCENT}" | awk '{ printf "%d", $1 * $2 }')
    
    JAVA_MAX_HEAP_PARAM="-Xmx${CONTAINER_HEAP_MAX}m"
else
    # default from https://gitlab.cern.ch/ai/it-puppet-module-flume/blob/qa/code/manifests/agent.pp
    JAVA_MAX_HEAP_PARAM="-Xmx100m"
fi 

if [[ $# -lt 1 ]]; then
    exec /usr/bin/aimon-flume-ng agent --classpath "${FLUME_CLASSPATH}" --conf "${FLUME_CONF_DIR}" --conf-file "${FLUME_CONF_FILE}" --name "${FLUME_AGENT_NAME}"  $JAVA_DNS_SETTINGS $JAVA_MAX_HEAP_PARAM $LOW_MEMORY_PARAMS $JAVA_OPTS
fi

# user provided arguments. Execute that instead of flume (e.g. bash)
exec "$@"

