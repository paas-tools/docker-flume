# Overview

An image for use in Openshift and other central web servers to collect
logs and send them to central monitoring.

Reference config: http://monitdocs.web.cern.ch/monitdocs/service_logs.html

# Deployment in Openshift

## HAProxy logs

Collect HAProxy logs including HTTP logs and send them to the `openshift` provider
with type `haproxy`. These logs can then be processed by WebServices to offer site
owners raw logs or statistics. Uses `syslog` over UDP, the only logging
facility supported by HAProxy.

For example deployments using this, see:
* https://gitlab.cern.ch/paas-tools/paas-infra/haproxy-logging (default HAProy router logging)
* https://gitlab.cern.ch/vcs/oo-gitlab-cern/ (GitLab application logs and GitLab HAProxy router logs)
