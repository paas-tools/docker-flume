FROM cern/cc7-base

ENV FLUME_AGENT_NAME=docker-flume \
    FLUME_CONF_DIR=/etc/aimon-flume-ng/conf/ \
    FLUME_CONF_FILE=/etc/aimon-flume-ng/conf/flume.conf \
    FLUME_CLASSPATH=/usr/share/java/* \
    JAVA_OPTS=-Duser.timezone=Europe/Zurich \
    JAVA_HOME=/usr/lib/jvm/jre-openjdk

# Limit vmem usage
# https://issues.apache.org/jira/browse/HADOOP-7154
ENV MALLOC_ARENA_MAX=4

# itmon repo 
COPY cern/itmon7-stable.repo /etc/yum.repos.d/

# install flume
RUN yum update -y && \
    yum install -y aimon-flume-ng itmon-flume-extra && \
    yum clean all

# standard grok dictionaries from https://gitlab.cern.ch/ai/it-puppet-module-flume/tree/qa/code/files/grok-dictionaries
COPY grok-dictionaries /etc/aimon-flume-ng/grok-dictionaries

# Flume conf files are expected to be provided by user as a docker volume
VOLUME /etc/aimon-flume-ng/conf

COPY run-flume.sh /
RUN chmod +x /run-flume.sh

ENTRYPOINT /run-flume.sh
